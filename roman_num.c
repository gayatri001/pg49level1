#include<stdio.h>
struct string
{
   char s[100];
};
typedef struct string str;
int no_of_inputs()
{
    int n;
    printf("enter the number of roman numbers you want to input\n");
    scanf("%d",&n);
    return n;
}
void input_n_str(int n,str h[n])
{
    for(int i=0;i<n;i++)
    {
        printf("enter the roman number\n");
        scanf("%s",h[i].s);
    }
}
void convert_roman(int n,str h[n],int a[n])
{
    int j=0;
    for(int i=0;i<n;i++)
    {
        a[i]=0;
        for(int j=0;h[i].s[j]!='\0';j++)
        {
            if(h[i].s[j]=='I')
            {
                if(h[i].s[j=j+1]=='V')
                {
                    a[i]=a[i]+4;
                    j=j+2;
                }
            }
        }
        for(int j=0;h[i].s[j]!='\0';j++)
        {
            if(h[i].s[j]=='X')
            {
                if(h[i].s[j=j+1]=='C')
                {
                    a[i]=a[i]+90;
                    j=j+2;
                }
                if(h[i].s[j=j+1]=='L')
                {
                    a[i]=a[i]+40;
                    j=j+2;
                }
                
                if(h[i].s[j=j+1]=='X')
                {
                    if(h[i].s[j=j+2]=='C')
                    {
                        a[i]=a[i]+80;
                        j=j+3;
                    }
                }
           }
           
        }
        for(int j=0;h[i].s[j]!='\0';j++)
        {
            if(h[i].s[j]=='C')
            {
                if(h[i].s[j=j+1]=='M')
                {
                    a[i]=a[i]+900;
                    j=j+2;
                }
                if(h[i].s[j++]=='D')
                {
                    a[i]=a[i]+400;
                    j=j+2;
                }
            }
        }
        for(int j=0;h[i].s[j]!='\0';j++)
        {
            if(h[i].s[j]=='I')
            {
                a[i]=a[i]+1;
                j=j+1;
            }
            if(h[i].s[j]=='V')
            {
                a[i]=a[i]+5;
                j=j+1;
            }
            if(h[i].s[j]=='L')
            {
                a[i]=a[i]+5;
                j=j+1;
            }
            if(h[i].s[j]=='C')
            {
                a[i]=a[i]+100;
                j=j+1;
            }
            if(h[i].s[j]=='D')
            {
                a[i]=a[i]+500;
                j=j+1;
            }
            if(h[i].s[j]=='M')
            {
                a[i]=a[i]+1000;
                j=j+1;
            }
            if(h[i].s[j]=='X')
            {
                a[i]=a[i]+10;
                j=j+1;
            }
        }
    }
        
    
}
void output(int n,str h[n],int a[n])
{
    for(int i=0;i<n;i++)
    {
        printf("the given roman number is %s",h[i].s);
        printf("\t = \t");
        printf("%d\n",a[i]);
    }
}
int main()
{
    int n;
    n=no_of_inputs();
    str h[n];
    input_n_str(n,h);
    int a[n];
    convert_roman(n,h,a);
    output(n,h,a);
    return 0;
}