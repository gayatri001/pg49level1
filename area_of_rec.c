#include<stdio.h>
#include<math.h>
struct coordinate
{
    float x1;
    float y1;
    float x2;
    float y2;
    float x3;
    float y3;
    float h[3];
};
typedef struct coordinate point; 
int no_of_rec()
{
    int n;
    printf("enter the number of rectancles whose area you want to calculate\n");
    scanf("%d",&n);
    return n;
}
void input_points(int n,point rec[n])
{
    for(int i=0;i<n;i++)
    {
        printf("enter the coordinates for the rectangle %d\n",(i+1));
        printf("enter the first set of coordinates\n");
        scanf("%f %f",&rec[i].x1,&rec[i].y1);
        printf("enter the second set of coordinates\n");
        scanf("%f %f",&rec[i].x2,&rec[i].y2);
        printf("enter the third set of coordinates\n");
        scanf("%f %f",&rec[i].x3,&rec[i].y3);
    }
}
float cal_length(int a,int b,int c,int d)
{
    float k;
    k=sqrt(pow((a-c),2)+pow((b-d),2));
    return k;
}
void cal_n_length(int n,point rec[n],float h[n][3])
{
    for(int i=0;i<n;i++)
    {
        h[i][0]=cal_length(rec[i].x1,rec[i].y1,rec[i].x2,rec[i].y2);
        h[i][1]=cal_length(rec[i].x2,rec[i].y2,rec[i].x3,rec[i].y3);
        h[i][2]=cal_length(rec[i].x3,rec[i].y3,rec[i].x1,rec[i].y1);
    }
}
void cal_area(int n,point rec[n],float h[n][3],float ar[n])
{
    for(int i=0;i<n;i++)
    {
        if(h[i][1]==h[i][2])
        {
            ar[i]=h[i][1]*h[i][3];
        }
        if(h[i][1]==h[i][3])
        {
            ar[i]=h[i][1]*h[i][2];
        }
        
    }
}
void output(int n,point rec[n],float ar[n])
{
    printf("there are %d rectancles given\n",n);
    for(int i=0;i<n;i++)
    {
        printf("the coordinates of the given rectangle are\n %d,%d\t %d,%d\t %d,%d\t",rec[i].x1,rec[i].y1,rec[i].x2,rec[i].y2,rec[i].x3,rec[i].y3);
        printf("\n");
        printf("the area of the rectangle is %f\n",ar[i]);
    }
}
int main()
{
    int n;
    n=no_of_rec();
    point rec[n];
    input_points(n,rec);
    float h[n][3];
    cal_n_length(n,rec,h);
    float ar[n];
    cal_area(n,rec,h,ar);
    output(n,rec,ar);
    return 0;
}
