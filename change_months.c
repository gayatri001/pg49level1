#include<stdio.h>
struct dates
{
    char s[100];
    int date;
    int year;
};
typedef struct dates Date;
int no_of_dates()
{
    int n;
    printf("enter the number of dates you want to input\n");
    scanf("%d",&n);
    return n;
}
void input_n_dates(int n,Date g[n])
{
    printf("enter the dates\n");
    printf("enter the dates in the form of month in words,date year\n");
    for(int i=0;i<n;i++)
    {
        printf("enter the date\n");
        scanf("%s%d%d",g[i].s,&g[i].date,&g[i].year);
    }
}
void change_months(int n,Date g[n],int a[n])
{
    for(int i=0;i<n;i++)
    {
        if(g[i].s[0]=='S')
        {
            a[i]=9;
        }
        if(g[i].s[0]=='O')
        {
            a[i]=10;
        }
        if(g[i].s[0]=='N')
        {
            a[i]=11;
        }
        if(g[i].s[0]=='D')
        {
            a[i]=12;
        }
        if(g[i].s[0]=='A')
        {
            if(g[i].s[1]=='p')
            {
                a[i]=4;
            }
            else
            {
                a[i]=8;
            }
        }
        if(g[i].s[0]=='M')
        {
            if(g[i].s[3]=='r')
            {
                a[i]=3;
            }
            else
            {
                a[i]=5;
            }
        }
        if(g[i].s[0]=='F')
        a[i]=2;
        if(g[i].s[0]=='J')
        {
            if(g[i].s[4]=='u')
            {
                a[i]=1;
            }
            if(g[i].s[4]=='y')
            {
                a[i]=7;
            }
            else
            {
                a[i]=6;
            }
        }
    }
}
void output_dates(int n,Date g[n],int a[n])
{
    printf("the dates with a changed format is as below\n");
    for(int i=0;i<n;i++)
    {
        printf("%s,%d %d",g[i].s,g[i].date,g[i].year);
        printf("\t:\t");
        printf("%d-%d-%d",g[i].year,a[i],g[i].date);
        printf("\n");
    }
}
int main()
{
    int n;
    n=no_of_dates();
    Date g[n];
    input_n_dates(n,g);
    int a[n];
    change_months(n,g,a);
    output_dates(n,g,a);
    return 0;
}