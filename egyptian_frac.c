#include<stdio.h>
struct fractions
{
    int k;
    int den[100];
    int num;
}; 
typedef struct fractions frac; 
int input_no_frac()
{
    int n;
    printf("enter the number of egyptian fractions you want to calculate\n");
    scanf("%d",&n);
    return n;
} 
void input_egyp_frac(int n,frac b[n])
{
    for(int i=0;i<n;i++)
    {
        printf("enter the number of unit fractions you want to add \n");
        scanf("%d",&b[i].k);
        for(int j=0;j<b[i].k;j++)
        {
            printf("enter the integer\n");
            scanf("%d",&b[i].den[j]);
        }
    }
}
void lcm_n_den(int n,frac b[n],int lcm[n])
{
    for(int i=0;i<n;i++)
    {
        lcm[i]=1;
        for(int j=0;j<b[i].k;j++)
        {
            lcm[i]=lcm[i]*b[i].den[j];
        }
    }
}
void compute_frac(int n,frac b[n],int lcm[n])
{
    for(int i=0;i<n;i++)
    {
        b[i].num=0;
        for(int j=0;j<b[i].k;j++)
        {
            b[i].num=b[i].num+(lcm[i]/b[i].den[j]);
        }
    }
}

void gcd_n_frac(int n,frac b[n],int gcd[n],int lcm[n])
{
    for(int i=0;i<n;i++)
    {
        
        for(int j=1;j<b[i].num && j<lcm[i];j++)
        {
            if(b[i].num%j==0 && lcm[i]%j==0)
            {
                gcd[i]=j;
            }
        }
    }
    
}
void reduce_frac(int n,frac b[n],int gcd[n],int h[n],int g[n],int lcm[n])
{
    for(int i=0;i<n;i++)
    {
        h[i]=(b[i].num)/gcd[i];
        g[i]=lcm[i]/gcd[i];
    }
}
void output_frac(int n,frac b[n],int h[n],int g[n])
{
    for(int i=0;i<n;i++)
    {
        printf("the given set of egyptian fractions are\n");
        for(int j=0;j<b[i].k;j++)
        {
            printf("%d\t",b[i].den[j]);
        }
        printf("\n");
        printf("the actual fraction is %d/%d\n",h[i],g[i]);
    }
    
}
int main()
{
    int n;
    n=input_no_frac();
    frac b[n];
    input_egyp_frac(n,b);
    int lcm[n];
    lcm_n_den(n,b,lcm);
    int num[n];
    compute_frac(n,b,lcm);
    int gcd[n];
    gcd_n_frac(n,b,gcd,lcm);
    int h[n];
    int g[n];
    reduce_frac(n,b,gcd,h,g,lcm);
    output_frac(n,b,h,g);
    return 0;
}


